# Maintainer: Martijn Braam <martijn@brixit.nl>

# This is seperate from linux-edge so we can control the release cycle better.
# If this kernel is updated and fails to boot it's quite hard to recover the 
# rk3399 devices due to the boot order. This kernel also has a kconfig that has
# more modules built-in that are required for successfully booting on these
# devices.

# It's important that CONFIG_ROCKCHIP_CDN_DP is _disabled_ because it causes
# boot failure

pkgname=linux-postmarketos-rockchip
pkgver=5.11.2
pkgrel=0
pkgdesc="Mainline kernel for rockchip devices"
arch="aarch64"
_carch="arm64"
_flavor="${pkgname#linux-}"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native pmb:kconfigcheck-anbox"
makedepends="
	bison
	devicepkg-dev
	findutils
	flex
	installkernel
	openssl-dev
	perl
	rsync
	xz
	"

# Source
_config="config-$_flavor.$arch"
case $pkgver in
	*.*.*)	_kernver=${pkgver%.0};;
	*.*)	_kernver=$pkgver;;
esac
source="
	https://cdn.kernel.org/pub/linux/kernel/v${_kernver%%.*}.x/linux-$_kernver.tar.xz
	$_config
"
builddir="$srcdir/linux-$_kernver"

prepare() {
	default_prepare
	REPLACE_GCCH=0 \
		. downstreamkernel_prepare
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	downstreamkernel_package "$builddir" "$pkgdir" "$_carch" "$_flavor"

	make -j1 modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs-$_flavor"
}


sha512sums="16090ec6dea7a8c417ca7483b296902c9b55b423482ad8a881dffcaae76411806bc9502373efd6a51b0acefec3a44c19c5a7d42c5b76c1321183a4798a5959d3  linux-5.11.2.tar.xz
b399eaaa7121b93468432e9f0a6d544199caa7c85b835c084fa24af7627d3e148a4f0e88eb703c3902f18e2df8202bf4a1e6d058a5901d4819b3673ef407266a  config-postmarketos-rockchip.aarch64"
