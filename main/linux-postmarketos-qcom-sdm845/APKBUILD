# Maintainer: Caleb Connolly <caleb@connolly.tech>
# Co-Maintainer: Joel Selvaraj <jo@jsfamily.in>
# Stable Linux kernel with patches for SDM845 devices
# Kernel config based on: arch/arm64/configs/defconfig and sdm845.config

_flavor="postmarketos-qcom-sdm845"
pkgname=linux-$_flavor
pkgver=5.11
pkgrel=2
pkgdesc="Mainline Kernel fork for SDM845 devices"
arch="aarch64"
_carch="arm64"
url="https://gitlab.com/sdm845-mainline/sdm845-linux"
license="GPL-2.0-only"
options="!strip !check !tracedeps pmb:cross-native"
makedepends="bison findutils flex installkernel openssl-dev perl"

_repo="sdm845-linux"
_config="config-$_flavor.$arch"
_commit="ccc42a98e25de2737f1b28edbdc6b0559c322815"

# Source
source="
	$_repo-$_commit.tar.gz::https://gitlab.com/sdm845-mainline/$_repo/-/archive/$_commit/$_repo-$_commit.tar.gz
	$_config
"
builddir="$srcdir/$_repo-$_commit"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$arch" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot/ \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir"/usr/share/dtb
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="157df965f733f1e9e6a9305ee9c90f7a550872f5d0acc8ac14439e1b870d9fc6a7be4de27afcc03f27b71f5891a4624c5d0d6c8664b2d045bc61581f4a5ae6b7  sdm845-linux-ccc42a98e25de2737f1b28edbdc6b0559c322815.tar.gz
0d8eae9997b5a132f669f87f137eac715f0b4d0a5d1c17973cab3a8ecef7026448f7eb352afbc0215752c1d506daa672bcf8ebe058221cbe57ba6bbcc75cb51a  config-postmarketos-qcom-sdm845.aarch64"
