# Reference: <https://postmarketos.org/devicepkg>
# Maintainer: Ruby Iris Juric <ruby@srxl.me>
# Co-Maintainer: Minecrell <minecrell@minecrell.net>
pkgname=device-motorola-harpia
pkgdesc="Motorola Moto G4 Play"
pkgver=4
pkgrel=0
url="https://postmarketos.org"
license="MIT"
arch="aarch64"
options="!check !archcheck"
depends="postmarketos-base mkbootimg soc-qcom-msm8916"
makedepends="devicepkg-dev"
source="deviceinfo"

subpackages="
	$pkgname-kernel-mainline:kernel_mainline
	$pkgname-kernel-mainline-modem:kernel_mainline_modem
	$pkgname-nonfree-firmware:nonfree_firmware
	$pkgname-nonfree-firmware-modem:nonfree_firmware_modem
"

build() {
	devicepkg_build $startdir $pkgname
}

package() {
	devicepkg_package $startdir $pkgname
}

kernel_mainline() {
	pkgdesc="Close to mainline kernel (no modem, audio routed directly)"
	depends="linux-postmarketos-qcom-msm8916"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

kernel_mainline_modem() {
	pkgdesc="Close to mainline kernel (non-free modem, audio routed through ADSP)"
	depends="linux-postmarketos-qcom-msm8916 soc-qcom-msm8916-modem"
	devicepkg_subpackage_kernel $startdir $pkgname $subpkgname
}

nonfree_firmware() {
	pkgdesc="GPU/Wi-Fi/Bluetooth/Video(/Modem) firmware"
	depends="linux-firmware-qcom firmware-motorola-harpia-wcnss firmware-motorola-harpia-venus"
	mkdir "$subpkgdir"
}

nonfree_firmware_modem() {
	pkgdesc="Modem firmware"
	depends="firmware-motorola-harpia-modem"
	install_if="$pkgname-nonfree-firmware $pkgname-kernel-mainline-modem"
	mkdir "$subpkgdir"
}

sha512sums="15d2edecf5a9d3a2e08cda6a9a0ced7fc1d26cf9f151a7fbb949f08bbbb152561ba025dd2982de8c601966ac6d4740d5f79379911570b1ec0ba12b4281a0eeb8  deviceinfo"
